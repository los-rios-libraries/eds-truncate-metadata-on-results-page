# README #

## NO LONGER UPDATED ##
This has been integrated into the EDS-Complete repository and any further changes will be made there.

* * *

From time to time on EDS results screens you'll see a horrendously long list of authors. EBSCO should fix this but so far has not. This script looks for long strings in that area and if it finds them, truncates them.

It goes in the results screen widget.

Before:

![long author string](https://bitbucket.org/repo/RyKaxR/images/1499155168-long-string.png)

After:

![truncated string](https://bitbucket.org/repo/RyKaxR/images/518696567-truncated-string.png)

## Issue / To Do ##

Unfortunately the string with the authors is not in its own div--it is in a div that then contains as children the divs with the format icon and the subjects. The solution is to extract those child divs and then append them to the truncated string. However, this creates some formatting irregularity because the children should actually precede the author string. Also, the search terms are enclosed in <strong> tags so they are technically children, and so they are appended after the truncated string... But this problem comes up rarely enough that I'm not sure it's worth the time to figure out a better solution...