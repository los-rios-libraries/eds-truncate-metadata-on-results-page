$('.display-info', window.parent.document).each(function() {
 children = $(this).children();
 mainText = $(this).clone().children().remove().end().text(); // see http://viralpatel.net/blogs/jquery-get-text-element-without-child-element/
 var shortText = $.trim(mainText).substring(0, 400)
    .split(" ").slice(0, -1).join(" ") + "...";
    if (mainText.length > 600) {
 $(this).text(shortText);
 $(this).append(children);
    } 
});


